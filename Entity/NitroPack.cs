﻿using Struct;
using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model.MiXa {
	public class NitroPack : Entity {
		public int RespawnTicks { get; }

		public NitroPack(Model.NitroPack pack) : base(pack) {
			RespawnTicks = pack.respawn_ticks == null ? 0 : (int)pack.respawn_ticks;
		}

		public NitroPack(NitroPack pack) : base(pack) {
			RespawnTicks = pack.RespawnTicks;
		}
	}
}