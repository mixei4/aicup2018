using Struct;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model.MiXa {
	public class Robot : Entity {
		public int Id {get;}
        public int PlayerId {get;}
        public bool IsTeammate {get;}
        public double NitroAmount {get; set;}
        public bool Touch {get; set;}
		public Point3 TouchNormal {get; set;}
		public Struct.Action Action;
		public bool CanUseNitro { get { return NitroAmount >= Consts.NITRO_PER_TICK; } }

		public Robot(Model.Robot robot) : base(robot) {
			Id = robot.id;
			PlayerId = robot.player_id;
			IsTeammate = robot.is_teammate;
			NitroAmount = robot.nitro_amount;
			Touch = robot.touch;
			if (Touch) {
				TouchNormal = new Point3((double)robot.touch_normal_x, (double)robot.touch_normal_z, (double)robot.touch_normal_y);
			}
		}

		public Robot(Robot robot) : base(robot) {
			Id = robot.Id;
			PlayerId = robot.PlayerId;
			IsTeammate = robot.IsTeammate;
			NitroAmount = robot.NitroAmount;
			Touch = robot.Touch;
			if (Touch) {
				TouchNormal = new Point3((double)robot.TouchNormal.X, (double)robot.TouchNormal.Y, (double)robot.TouchNormal.Z);
			}
		}
	}
}