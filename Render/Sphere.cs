using Struct;

namespace Render {
	public class Sphere : Figure {
		public double x, y, z, radius, r, g, b, a;
		public Sphere(double x, double y, double z, double radius, double r = 1, double g = 0, double b = 0, double a = 0.5) {
			this.x = x;
			this.y = z;
			this.z = y;
			this.radius = radius;
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
		}

		public Sphere(Point3 p, double radius, double r = 1, double g = 0, double b = 0, double a = 0.5) : this(p.X, p.Y, p.Z, radius, r, g, b, a) { }
	}
}