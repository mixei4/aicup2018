using System;
using System.Linq;

namespace Ext {
	public static class Ext {
		public static bool In<T>(this T val, params T[] values) where T : struct {
			return values.Contains(val);
		}

		public static bool Eq(this double q, double w) {
			return Math.Abs(q - w) < MathMiXa.Eps;
		}

		public static bool More(this double q, double w) {
			return q - w > MathMiXa.Eps;
		}

		public static bool MoreEq(this double q, double w) {
			return q.More(w) || q.Eq(w);
		}

		public static bool Less(this double q, double w) {
			return w.More(q);
		}

		public static bool LessEq(this double q, double w) {
			return w.MoreEq(q);
		}

		public static bool Between(this double q, double w, double e) {
			return q.More(w) && q.Less(e);
		}

		public static bool BetweenEq(this double q, double w, double e) {
			return q.MoreEq(w) && q.LessEq(e);
		}

		public static bool Between(this int q, int w, int e) {
			return q > w && q < e;
		}
		
		public static bool BetweenEq(this int q, int w, int e) {
			return q >= w && q <= e;
		}

		public static double Clamp(this double q, double l, double r) {
			return Math.Min(Math.Max(q, l), r);
		}

		public static double Sqr(this int q) {
			return q * q;
		}

		public static double Sqr(this double q) {
			return q * q;
		}
	}
}