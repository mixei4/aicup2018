using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model.MiXa;
using System;
using Ext;
using Struct;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk {
	public class Simulator {
		private Robot[] robots;
		public Robot[] Robots { get { return robots; } }
		private Ball ball;
		public Ball Ball { get { return ball; } }
		private Model.Arena arena;
		private int ticks = 0;
		public int Ticks {get {return ticks;}}
		private int microTicks = 0;
		public int MicroTicks {get {return microTicks;}}
		private int microTicksCount = 1;
		public int MicroTicksCount {get {return microTicksCount;}}

		public Simulator(Model.Arena arena, Robot[] robots, Ball ball, int microTicksCount = 2) {
			this.ball = ball == null ? null : new Ball(ball);
			this.robots = new Robot[robots.Length];
			for (int i = 0; i < robots.Length; i++) {
				this.robots[i] = new Robot(robots[i]);
			}
			this.arena = arena;
			this.microTicksCount = Math.Min(100, microTicksCount + 1);
		}

		public Simulator(Model.Arena arena, Model.Game game, int microTicksCount = 2) {
			this.ball = new Ball(game.ball);
			this.robots = new Robot[game.robots.Length];
			for (int i = 0; i < game.robots.Length; i++) {
				this.robots[i] = new Robot(game.robots[i]);
			}
			this.arena = arena;
			this.microTicksCount = Math.Min(100, microTicksCount + 1);
		}

		public void SetMicroTicksCount(int count) {
			this.microTicksCount = count;
		}

		public void SetBall(Ball ball) {
			this.ball = ball;
		}

		public void RemoveRobots() {
			robots = new Robot[0];
		}

		public Robot GetRobot(int id) {
			foreach (var Robot in Robots) {
				if (Robot.Id == id) {
					return Robot;
				}
			}
			return null;
		}

		public double GetRadiusChangeSpeed(double r) {
			if (r.More(Consts.ROBOT_MAX_RADIUS)) {
				return 0;
			}
			var t = (r - Consts.ROBOT_MIN_RADIUS) * Consts.ROBOT_MAX_JUMP_SPEED;
			t = t / (Consts.ROBOT_MAX_RADIUS - Consts.ROBOT_MIN_RADIUS);
			return t / Consts.TICKS_PER_SECOND;
		}

		private void CollideEntities(Entity a, Entity b) {
			if (b == null) {
				return;
			}
			var delta_position = b.Pos - a.Pos;
			var distance = delta_position.Dist();
			var penetration = (a.R + b.R - distance);
			if (penetration > 0) {
				var k_a = (1 / a.M) / ((1 / a.M) + (1 / b.M));
				var k_b = (1 / b.M) / ((1 / a.M) + (1 / b.M));
				var normal = delta_position.Normalize();
				a.Pos -= normal * penetration * k_a;
				b.Pos += normal * penetration * k_b;
				var delta_velocity = (b.Speed - a.Speed) * normal - GetRadiusChangeSpeed(b.R) - GetRadiusChangeSpeed(a.R);
				if (delta_velocity < 0) {
					var impulse = normal * (1 + (Consts.MIN_HIT_E + Consts.MAX_HIT_E) / 2) * delta_velocity;
					a.Speed += impulse * k_a;
					b.Speed -= impulse * k_b;
				}
			}
		}

		private Point3? CollideWithArena(Entity e) {
			if (e == null) {
				return null;
			}
			var dan = DanToArena(e.Pos);
			var penetration = (e.R - dan.Dist);
			if (penetration > 0) {
				e.Pos += dan.Normal * penetration;
				var velocity = e.Speed * dan.Normal - GetRadiusChangeSpeed(e.R);
				if (velocity < 0) {
					var arena_e = e is Robot ? Consts.ROBOT_ARENA_E : Consts.BALL_ARENA_E;
					e.Speed -= dan.Normal * (1 + arena_e) * velocity;
					return dan.Normal;
				}
			}
			return null;
		}

		private void Move(Entity e, double delta) {
			if (e == null) {
				return;
			}
			e.Speed = e.Speed.Clamp(Consts.MAX_ENTITY_SPEED);
			e.Pos += e.Speed * delta;
			e.Z -= Consts.GRAVITY * delta * delta / 2;
			e.Speed -= new Point3(0, 0, Consts.GRAVITY * delta);
		}

		private void UpdateSpeed(Robot robot, double delta) {
			if (robot.Action == null) {
				return;
			}
			if (robot.Touch) {
				var target_velocity = robot.Action.TargetVelocity.Clamp(Consts.ROBOT_MAX_GROUND_SPEED);
				target_velocity -= robot.TouchNormal * (robot.TouchNormal * target_velocity);
				var target_velocity_change = target_velocity - robot.Speed;
				if (target_velocity_change.Dist() > 0) {
					var acceleration = Consts.ROBOT_ACCELERATION * Math.Max(0, robot.TouchNormal.Z);
					robot.Speed += (target_velocity_change.Normalize() * acceleration * delta).Clamp(target_velocity_change.Dist());
				}
			}
			if (robot.Action.UseNitro) {
				var target_velocity_change = (robot.Action.TargetVelocity - robot.Speed).Clamp(robot.NitroAmount * Consts.NITRO_POINT_VELOCITY_CHANGE);
				if (target_velocity_change.Dist() > 0) {
					var acceleration = target_velocity_change.Normalize() * Consts.ROBOT_NITRO_ACCELERATION;
					var velocity_change = (acceleration * delta).Clamp(target_velocity_change.Dist());
					robot.Speed += velocity_change;
					robot.NitroAmount -= velocity_change.Dist() / Consts.NITRO_POINT_VELOCITY_CHANGE;
				}
			}
		}

		public void Update(int ticks = 1, int microTicks = 0) {
			microTicks = ticks * this.microTicksCount + microTicks;
			for (int _ = 0; _ < microTicks; _++) {
				var delta = 0.99 / (this.microTicksCount - 1);
				if (this.microTicks == 0) {
					delta = 0.01;
				}
				foreach (var robot in Robots) {
					UpdateSpeed(robot, delta);
					Move(robot, delta);
				}
				Move(Ball, delta);

				for (int i = 0; i < Robots.Length; i++) {
					for (int j = 0; j < i; j++) {
						CollideEntities(Robots[i], Robots[j]);
					}
				}
				foreach (var robot in Robots) {
					CollideEntities(robot, Ball);
					var collision_normal = CollideWithArena(robot);
					if (collision_normal == null) {
						robot.Touch = false;
					} else {
						robot.Touch = true;
						robot.TouchNormal = (Point3)collision_normal;
					}
				}
				CollideWithArena(Ball);

				this.microTicks++;
				if (this.microTicks == this.microTicksCount) {
					this.ticks++;
					this.microTicks = 0;
				}
			}
		}

		private Dan dan_to_plane(Point3 point, Point3 point_on_plane, Point3 plane_normal) {
			return new Dan {
				Dist = (point - point_on_plane) * plane_normal,
				Normal = plane_normal
			};
		}
		
		private Dan dan_to_sphere_inner(Point3 point, Point3 sphere_center, double sphere_radius) {
			return new Dan {
				Dist = sphere_radius - (point - sphere_center).Dist(),
				Normal = (sphere_center - point).Normalize()
			};
		}

		private Dan dan_to_sphere_outer(Point3 point, Point3 sphere_center, double sphere_radius) {
			return new Dan {
				Dist = (point - sphere_center).Dist() - sphere_radius,
				Normal = (point - sphere_center).Normalize()
			};
		}

		private Dan min(Dan q, Dan w) {
			if (q.Dist < w.Dist) {
				return q;
			}
			return w;
		}

		private double clamp(double d, double q, double w) {
			if (d < q) {
				d = q;
			}
			if (d > w) {
				d = w;
			}
			return d;
		}

		private Dan DanToArenaQuarter(Point3 point) {
			Point2 v;
			// Ground
			var dan = dan_to_plane(point, new Point3(0, 0, 0), new Point3(0, 0, 1));
			// Ceiling
			dan = min(dan, dan_to_plane(point, new Point3(0, 0, arena.height), new Point3(0, 0, -1)));
			// Side x
			dan = min(dan, dan_to_plane(point, new Point3(arena.width / 2, 0, 0), new Point3(-1, 0, 0)));
			// Side z (goal)
			dan = min(dan, dan_to_plane(point, new Point3(0, (arena.depth / 2) + arena.goal_depth, 0), new Point3(0, -1, 0)));
			// Side z
			v = new Point2(point.X, point.Z) - new Point2(
				(arena.goal_width / 2) - arena.goal_top_radius,
				arena.goal_height - arena.goal_top_radius
			);
			if (
				point.X >= (arena.goal_width / 2) + arena.goal_side_radius || point.Z >= arena.goal_height + arena.goal_side_radius || 
				(v.X > 0 && v.Y > 0 && v.Dist() >= arena.goal_top_radius + arena.goal_side_radius)
			) {
				dan = min(dan, dan_to_plane(point, new Point3(0, arena.depth / 2, 0), new Point3(0, -1, 0)));
			}
			// Side x & ceiling (goal)
			if (point.Y >= (arena.depth / 2) + arena.goal_side_radius) {
				// x
				dan = min(dan, dan_to_plane(point, new Point3(arena.goal_width / 2, 0, 0), new Point3(-1, 0, 0)));
				// y
				dan = min(dan, dan_to_plane(point, new Point3(0, 0, arena.goal_height), new Point3(0, 0, -1)));
			}
			// Goal back corners
			if (point.Y > (arena.depth / 2) + arena.goal_depth - arena.bottom_radius) {
				dan = min(dan, dan_to_sphere_inner(
					point,
					new Point3(
						clamp(point.X, arena.bottom_radius - (arena.goal_width / 2), (arena.goal_width / 2) - arena.bottom_radius),
						(arena.depth / 2) + arena.goal_depth - arena.bottom_radius,
						clamp(point.Z, arena.bottom_radius, arena.goal_height - arena.goal_top_radius)						
					),
					arena.bottom_radius));
			}
			// Corner
			if (point.X > (arena.width / 2) - arena.corner_radius && point.Y > (arena.depth / 2) - arena.corner_radius) {
				dan = min(dan, dan_to_sphere_inner(
					point,
					new Point3(
						(arena.width / 2) - arena.corner_radius,
						(arena.depth / 2) - arena.corner_radius,
						point.Z
					),
					arena.corner_radius));
			}
			// Goal outer corner
			if (point.Y < (arena.depth / 2) + arena.goal_side_radius) {
				// Side x
				if (point.X < (arena.goal_width / 2) + arena.goal_side_radius) {
					dan = min(dan, dan_to_sphere_outer(
					point,
					new Point3(
						(arena.goal_width / 2) + arena.goal_side_radius,
						(arena.depth / 2) + arena.goal_side_radius,
						point.Z
					),
					arena.goal_side_radius));
				}
				// Ceiling
				if (point.Z < arena.goal_height + arena.goal_side_radius) {
					dan = min(dan, dan_to_sphere_outer(
					point,
					new Point3(
						point.X,
						(arena.depth / 2) + arena.goal_side_radius,
						arena.goal_height + arena.goal_side_radius
					),
					arena.goal_side_radius));
				}
				// Top corner
				var o = new Point2(
					(arena.goal_width / 2) - arena.goal_top_radius,
					arena.goal_height - arena.goal_top_radius
				);
				v = new Point2(point.X, point.Z) - o;
				if (v.X > 0 && v.Y > 0) {
					o += v.Normalize() * (arena.goal_top_radius + arena.goal_side_radius);
					dan = min(dan, dan_to_sphere_outer(
						point,
						new Point3(o.X, (arena.depth / 2) + arena.goal_side_radius, o.Y),
						arena.goal_side_radius)
					);
				}
			}
			// Goal inside top corners
			if (point.Y > (arena.depth / 2) + arena.goal_side_radius && point.Z > arena.goal_height - arena.goal_top_radius) {
				// Side x
				if (point.X > (arena.goal_width / 2) - arena.goal_top_radius) {
					dan = min(dan, dan_to_sphere_inner(
						point,
						new Point3(
							(arena.goal_width / 2) - arena.goal_top_radius,
							point.Y,
							arena.goal_height - arena.goal_top_radius
						),
						arena.goal_top_radius));
				}
				// Side z
				if (point.Y > (arena.depth / 2) + arena.goal_depth - arena.goal_top_radius) {
					dan = min(dan, dan_to_sphere_inner(
					point,
					new Point3(
						point.X,
						(arena.depth / 2) + arena.goal_depth - arena.goal_top_radius,
						arena.goal_height - arena.goal_top_radius
					),
					arena.goal_top_radius));
				}
			}
			// Bottom corners
			if (point.Z < arena.bottom_radius) {
				// Side x
				if (point.X > (arena.width / 2) - arena.bottom_radius) {
					dan = min(dan, dan_to_sphere_inner(
					point,
					new Point3(
						(arena.width / 2) - arena.bottom_radius,
						point.Y,
						arena.bottom_radius
					),
					arena.bottom_radius));
				}
				// Side z
				if (point.Y > (arena.depth / 2) - arena.bottom_radius && point.X >= (arena.goal_width / 2) + arena.goal_side_radius) {
					dan = min(dan, dan_to_sphere_inner(
					point,
					new Point3(
						point.X,
						(arena.depth / 2) - arena.bottom_radius,
						arena.bottom_radius
					),
					arena.bottom_radius));
				}
				// Side z (goal)
				if (point.Y > (arena.depth / 2) + arena.goal_depth - arena.bottom_radius) {
					dan = min(dan, dan_to_sphere_inner(
					point,
					new Point3(
						point.X,
						(arena.depth / 2) + arena.goal_depth - arena.bottom_radius,
						arena.bottom_radius
					),
					arena.bottom_radius));
				}
				// Goal outer corner
				var o = new Point2(
					(arena.goal_width / 2) + arena.goal_side_radius,
					(arena.depth / 2) + arena.goal_side_radius
				);
				v = new Point2(point.X, point.Y) - o;
				if (v.X < 0 && v.Y < 0 && v.Dist() < arena.goal_side_radius + arena.bottom_radius) {
					o += v.Normalize() * (arena.goal_side_radius + arena.bottom_radius);
					dan = min(dan, dan_to_sphere_inner(
						point,
						new Point3(o.X, o.Y, arena.bottom_radius),
						arena.bottom_radius)
					);
				}
				// Side x (goal)
				if (point.Y >= (arena.depth / 2) + arena.goal_side_radius && point.X > (arena.goal_width / 2) - arena.bottom_radius) {
					dan = min(dan, dan_to_sphere_inner(
					point,
					new Point3(
						(arena.goal_width / 2) - arena.bottom_radius,
						point.Y,
						arena.bottom_radius
					),
					arena.bottom_radius));
				}
				// Corner
				if (point.X > (arena.width / 2) - arena.corner_radius && point.Y > (arena.depth / 2) - arena.corner_radius) {
					var corner_o = new Point2(
						(arena.width / 2) - arena.corner_radius,
						(arena.depth / 2) - arena.corner_radius
					);
					var n = new Point2(point.X, point.Y) - corner_o;
					var dist = n.Dist();
					if (dist > arena.corner_radius - arena.bottom_radius) {
						n = n / dist;
						var o2 = corner_o + n * (arena.corner_radius - arena.bottom_radius);
						dan = min(dan, dan_to_sphere_inner(
							point,
							new Point3(o2.X, o2.Y, arena.bottom_radius),
							arena.bottom_radius)
						);
					}
				}
			}
			// Ceiling corners
			if (point.Z > arena.height - arena.top_radius) {
				// Side x
				if (point.X > (arena.width / 2) - arena.top_radius) {
					dan = min(dan, dan_to_sphere_inner(
					point,
					new Point3(
						(arena.width / 2) - arena.top_radius,
						point.Y,
						arena.height - arena.top_radius
					),
					arena.top_radius));
				}
				// Side z
				if (point.Y > (arena.depth / 2) - arena.top_radius) {
					dan = min(dan, dan_to_sphere_inner(
					point,
					new Point3(
						point.X,
						(arena.depth / 2) - arena.top_radius,
						arena.height - arena.top_radius
					),
					arena.top_radius));
				}
				// Corner
				if (point.X > (arena.width / 2) - arena.corner_radius && point.Y > (arena.depth / 2) - arena.corner_radius) {
					var corner_o = new Point2(
						(arena.width / 2) - arena.corner_radius,
						(arena.depth / 2) - arena.corner_radius
					);
					var dv = new Point2(point.X, point.Y) - corner_o;
					if (dv.Dist() > arena.corner_radius - arena.top_radius) {
						var n = dv.Normalize();
						var o2 = corner_o + n * (arena.corner_radius - arena.top_radius);
						dan = min(dan, dan_to_sphere_inner(
							point,
							new Point3(o2.X, o2.Y, arena.height - arena.top_radius),
							arena.top_radius)
						);
					}
				}
			}
			return dan;
		}

		private Dan DanToArena(Point3 point) {
			var negate_x = point.X < 0;
			var negate_y = point.Y < 0;
			if (negate_x) {
				point.X = -point.X;
			}
			if (negate_y) {
				point.Y = -point.Y;
			}
			var result = DanToArenaQuarter(point);
			if (negate_x) {
				result.Normal.X = -result.Normal.X;
			}
			if (negate_y) {
				result.Normal.Y = -result.Normal.Y;
			}
			return result;
		}
	}
}