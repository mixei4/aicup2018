using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model.MiXa;

namespace Struct {
	public class Snapshot {
		public Robot[] Robots;
		public Ball Ball;

		public Snapshot(Robot[] robots, Ball ball) {
			Robots = new Robot[robots.Length];
			for (int i = 0; i < robots.Length; i++) {
				Robots[i] = new Robot(robots[i]);
			}
			Ball = new Ball(ball);
		}

		public Snapshot(Ball ball, Robot[] robots) : this(robots, ball) { }
	}
}