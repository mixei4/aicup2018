using System;
using Ext;

namespace Struct {
	public struct Point2 {
		public double X, Y;

		public Point2(double x, double y) {
			this.X = x;
			this.Y = y;
		}

		public Point2(Point3 p) {
			this.X = p.X;
			this.Y = p.Y;
		}

		public override string ToString() {
			return X.ToString() + ' ' + Y.ToString();
		}

		public static Point2 operator +(Point2 q, Point2 w) {
			return new Point2(q.X + w.X, q.Y + w.Y);
		}

		public static Point2 operator -(Point2 q, Point2 w) {
			return new Point2(q.X - w.X, q.Y - w.Y);
		}

		public static Point2 operator *(Point2 q, double w) {
			return new Point2(q.X * w, q.Y * w);
		}

		public static Point2 operator /(Point2 q, double w) {
			return new Point2(q.X / w, q.Y / w);
		}

		public static double operator *(Point2 q, Point2 w) {
			return q.X * w.X + q.Y * w.Y;
		}

		public static bool operator >(Point2 q, Point2 w) {
			return q.X.More(w.X) && q.Y.More(w.Y);
		}

		public static bool operator <(Point2 q, Point2 w) {
			return q.X.Less(w.X) && q.Y.Less(w.Y);
		}

		public bool Eq(Point2 p) {
			return this.Dist(p).Eq(0);
		}

		public bool Between(Point2 q, Point2 w) {
			return this.X.Between(q.X, w.X) && this.Y.Between(q.Y, w.Y);
		}

		public double Dist() {
			return Math.Sqrt(this.X * this.X + this.Y * this.Y);
		}

		public double Dist(Point2 p) {
			var q = this - p;
			return q.Dist();
		}

		// Distance to the line p1-p2
		public double Dist(Point2 p1, Point2 p2) {
			double r = p1.Dist(p2);
			return Math.Abs(MathMiXa.SSignedDoubled(this, p1, p2)) / r;
		}

		public double Dist(Segment2 s) {
			return s.Dist(this);
		}

		public double Dist2() {
			return this.X * this.X + this.Y * this.Y;
		}

		public Point2 Normalize() {
			var d = Dist();
			return new Point2(this.X / d, this.Y / d);
		}

		public bool CloseTo(Point2 p, double d = 0.1) {
			return this.Dist(p).Less(d);
		}

		public Point2 Clamp(double l) {
			if (this.Dist().More(l)) {
				return this.Normalize() * l;
			}
			return this;
		}
	}
}