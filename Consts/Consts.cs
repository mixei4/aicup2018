namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
	public static class Consts
	{
		public const double EPS = 1e-5;
		public const double ROBOT_MIN_RADIUS = 1.0;
		public const double ROBOT_MAX_RADIUS = 1.05;
		public const double ROBOT_MAX_JUMP_SPEED = 15.0;
		public const double ROBOT_ACCELERATION = 100.0 / TICKS_PER_SECOND / TICKS_PER_SECOND;
		public const double ROBOT_NITRO_ACCELERATION = 30.0 / TICKS_PER_SECOND / TICKS_PER_SECOND;
		public const double ROBOT_MAX_GROUND_SPEED = 30.0 / TICKS_PER_SECOND;
		public const double ROBOT_ARENA_E = 0;
		public const double ROBOT_RADIUS = 1.0;
		public const double ROBOT_MASS = 2.0;
		public const int TICKS_PER_SECOND = 60;
		public const int MICROTICKS_PER_TICK = 100;
		public const int RESET_TICKS = 2 * TICKS_PER_SECOND;
		public const double BALL_ARENA_E = 0.7;
		public const double BALL_RADIUS = 2.0;
		public const double BALL_MASS = 1.0;
		public const double MIN_HIT_E = 0.4;
		public const double MAX_HIT_E = 0.5;
		public const double MAX_ENTITY_SPEED = 100.0 / TICKS_PER_SECOND;
		public const double MAX_NITRO_AMOUNT = 100.0;
		public const double START_NITRO_AMOUNT = 50.0;
		public const double NITRO_POINT_VELOCITY_CHANGE = 0.6 / TICKS_PER_SECOND;
		public const double NITRO_PACK_X = 20.0;
		public const double NITRO_PACK_Y = 1.0;
		public const double NITRO_PACK_Z = 30.0;
		public const double NITRO_PACK_RADIUS = 0.5;
		public const double NITRO_PACK_AMOUNT = 100.0;
		public const int NITRO_RESPAWN_TICKS = 10 * TICKS_PER_SECOND;
		public const double GRAVITY = 30.0 / TICKS_PER_SECOND / TICKS_PER_SECOND;
		public const double NITRO_PER_TICK = 5 / 6;
		public const int MAX_NITRO_TICKS = 15;
	}
}